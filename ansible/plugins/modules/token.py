#!/usr/bin/env python3
# Copyright (C) 2022 Julien Lecomte <julien@lecomte.at>
# SPDX-License-Identifier: GPL-3.0-only


from ansible_collections.julien_lecomte.proxmox.plugins.module_utils.pvesh import ProxmoxShell

DOCUMENTATION = r"""
---
module: token
plugin_type: module
author: Julien Lecomte (julien@lecomte.at)
short_description: Adds, modifies, or removes a Proxmox user token.
description:
  - Adds, modifies, or removes a Proxmox user token.
  - Returned values will exist in a variable named 'token'.
seealso:
  - name: pveum man page
    description: pveum - Proxmox VE User Manager
    link: https://pve.proxmox.com/pve-docs/pveum.1.html
options:
  name:
    description:
      - The token name.
    type: str
    required: true
  user:
    description:
      - "The user name with realm (eg: user@pam)."
    type: str
    required: true
  state:
    description:
      - Specify if the token should exist (present) or absent.
    type: str
    choices: [ absent, present ]
    default: present
  comment:
    description:
      - Optionally sets the comment field.
    type: str
  expire:
    description:
      - Account expiration date (seconds since epoch). 0 means no expiration date (default on creation).
    type: int
  privsep:
    description:
      - Restrict API token privileges with separate ACLs (default), or give full privileges of corresponding user.
    type: bool
"""

EXAMPLES = r"""
- name: Create token 'example'
  julien_lecomte.proxmox.token:
    name: 'example'
    user: 'user01'
"""

RETURN = r"""
name:
  description: Proxmox token id.
  returned: always
  type: str
state:
  description: State ("absent" or "present").
  returned: always
  type: str
comment:
  description: Comment field.
  returned: when token exists
  type: str
expire:
  description: Token expiration date (seconds since epoch). 0 means no expiration date.
  returned: when token exists
  type: int
privsep:
  description: Whether API token privileges are separated or not.
  returned: when token exists
  type: bool
value:
  description: API token value
  returned: only on token creation
  type: str
"""


class ProxmoxToken(ProxmoxShell):
    def on_create_cmdline(self, cmd):
        return self.on_modify_cmdline(cmd)

    def on_modify_cmdline(self, cmd):
        if self.params.get("expire") and self.params["expire"] < 0:
            self.fail_json(msg="expire must be 0 or a positive integer")

        for key in [
            "comment",
            "expire",
            "privsep",
        ]:
            cmd.extend(self.maybe_command(key))
        return cmd


def main():
    module = ProxmoxToken(
        argument_spec=dict(
            name=dict(type="str", required=True),
            user=dict(type="str", required=True),
            state=dict(type="str", default="present", choices=["absent", "present"]),
            comment=dict(type="str"),
            privsep=dict(type="bool"),
            expire=dict(type="int"),
        ),
        no_log=True,
        supports_check_mode=False,
    )
    userid = module.params["user"]
    module.params["tokenid"] = module.params["name"]
    module.run(f"/access/users/{userid}/token/{module.params['name']}")
    module.exit_json(changed=module.changed, warnings=module.warnings, token=module.data)


if __name__ == "__main__":
    main()
