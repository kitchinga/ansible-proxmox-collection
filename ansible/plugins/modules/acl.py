#!/usr/bin/env python3
# Copyright (C) 2022 Julien Lecomte <julien@lecomte.at>
# SPDX-License-Identifier: GPL-3.0-only

from ansible_collections.julien_lecomte.proxmox.plugins.module_utils.pvesh import ProxmoxShell

DOCUMENTATION = r"""
---
module: acl
plugin_type: module
author: Julien Lecomte (julien@lecomte.at)
short_description: Adds, modifies, or removes a Proxmox ACL.
description:
  - Adds, modifies, or removes a Proxmox ACL.
  - Returned values will exist in a list named 'acls'.
seealso:
  - name: pveum man page
    description: pveum - Proxmox VE User Manager
    link: https://pve.proxmox.com/pve-docs/pveum.1.html
options:
  path:
    description:
      - The ACL path.
    type: str
    required: true
  state:
    description:
      - Specify if the ACL should exist (present) or absent.
    type: str
    choices: [ absent, present ]
    default: present
  groups:
    description:
      - List of groups.
    type: list
    elements: str
  propagate:
    description:
      - Allow to propagate (inherit) permissions.
    type: bool
  roles:
    description:
      - List of roles.
    type: list
    required: true
    elements: str
  tokens:
    description:
      - List of tokens.
    type: list
    elements: str
  users:
    description:
      - List of users.
    type: list
    elements: str
"""

EXAMPLES = r"""
- name: Create ACL.
  julien_lecomte.proxmox.acl:
    path: "/"
    roles: [ "PVEAdmin" ]
    groups: [ "admins" ]
"""

RETURN = r"""
name:
  description: Name of the group, token, or user.
  returned: always
  type: str
path:
  description: ACL path.
  returned: always
  type: str
state:
  description: State ("absent" or "present").
  returned: always
  type: str
propagate:
  description: Propagate field.
  returned: always
  type: int
role:
  description: Role name.
  returned: always
  type: str
type:
  description: Type (group, token, user).
  returned: always
  type: str
"""


class ProxmoxACL(ProxmoxShell):
    def on_detect_action(self):
        if self.params["state"] == "absent" and self.state == "absent":
            return
        self.do_modify_cmdline()
        # TODO: Do diff between list now and list before
        self.changed = True

    def _on_load(self):
        # self.data is a list, we can't add "state" to it so use a variable instead
        self.state = "present" if self.data else "absent"

    def on_load(self):
        # Filter on path
        self.data = [x for x in self.data if x["path"] == self.params["path"]]
        self.data = [x for x in self.data if x["type"] == self.item_type]

    def on_create_cmdline(self, cmd):
        return self.on_modify_cmdline(cmd)

    def on_modify_cmdline(self, cmd):
        cmd.extend(["-path", self.params["path"]])
        cmd.extend(["-propagate", "1" if self.params["propagate"] else "0"])
        cmd.extend(["-delete", "1" if self.params["state"] == "absent" else "0"])

        for item in ["roles", "groups", "users", "tokens"]:
            items = ",".join(self.params.get(item) or [])
            if items:
                cmd.extend([f"-{item}", items])

        return cmd


def main():
    module = ProxmoxACL(
        argument_spec=dict(
            path=dict(type="str", required=True),
            roles=dict(type="list", elements="str", required=True),
            state=dict(type="str", default="present", choices=["absent", "present"]),
            groups=dict(type="list", elements="str"),
            propagate=dict(type="bool", default=True),
            tokens=dict(type="list", elements="str"),
            users=dict(type="list", elements="str"),
        ),
        supports_check_mode=False,
    )

    module.changed = False
    module.warnings = []
    data = []

    for module.item_type in ["group", "user", "token"]:
        module.run("/access/acl")
    module.data = data
    module.exit_json(changed=module.changed, warnings=module.warnings, acls=module.data)


if __name__ == "__main__":
    main()
