#!/usr/bin/env python3
# Copyright (C) 2022 Julien Lecomte <julien@lecomte.at>
# SPDX-License-Identifier: GPL-3.0-only

from ansible_collections.julien_lecomte.proxmox.plugins.module_utils.pvesh import ProxmoxShell

DOCUMENTATION = r"""
---
module: pool
plugin_type: module
author: Julien Lecomte (julien@lecomte.at)
short_description: Adds, modifies, or removes a Proxmox pool.
description:
  - Adds, modifies, or removes a Proxmox pool.
  - Returned values will exist in a variable named 'pool'.
seealso:
  - name: PVE API
    description: Proxmox VE Application Programming Interface
    link: https://pve.proxmox.com/pve-docs/api-viewer/#/pools
options:
  name:
    description:
      - The pool name.
    type: str
    required: true
  state:
    description:
      - Specify if the pool should exist (present) or absent.
    type: str
    choices: [ absent, present ]
    default: present
  comment:
    description:
      - Optionally sets the comment field.
    type: str
"""

EXAMPLES = r"""
- name: Create pool 'example'
  julien_lecomte.proxmox.pool:
    name: 'example'

- name: Create pool 'example' with a comment
  julien_lecomte.proxmox.pool:
    name: 'example'
    comment: 'Lorem ipsum'
"""

RETURN = r"""
name:
  description: Proxmox pool name.
  returned: always
  type: str
state:
  description: State ("absent" or "present")
  returned: always
  type: str
comment:
  description: Comment field.
  returned: when pool exists
  type: str
"""


class ProxmoxPool(ProxmoxShell):
    def on_create_cmdline(self, cmd):
        cmd.extend(["-poolid", self.params["name"]])
        return self.on_modify_cmdline(cmd)

    def on_modify_cmdline(self, cmd):
        cmd.extend(self.maybe_command("comment"))
        return cmd


def main():
    module = ProxmoxPool(
        argument_spec=dict(
            name=dict(type="str", required=True),
            state=dict(type="str", default="present", choices=["absent", "present"]),
            comment=dict(type="str"),
        ),
        supports_check_mode=False,
    )
    module.params["poolid"] = module.params["name"]
    module.run(f"/pools/{module.params['name']}", "/pools")
    module.exit_json(changed=module.changed, warnings=module.warnings, pool=module.data)


if __name__ == "__main__":
    main()
