#!/usr/bin/python
# Copyright (C) 2022 Julien Lecomte <julien@lecomte.at>
# SPDX-License-Identifier: GPL-3.0-only


from ansible_collections.julien_lecomte.proxmox.plugins.module_utils.pvesh import ProxmoxShell

DOCUMENTATION = r"""
---
module: role
plugin_type: module
author: Julien Lecomte (julien@lecomte.at)
short_description: Adds, modifies, or removes a Proxmox role.
description:
  - Adds, modifies, or removes a Proxmox role.
  - Returned values will exist in a variable named 'role'.
seealso:
  - name: PVE API
    description: Proxmox VE Application Programming Interface
    link: https://pve.proxmox.com/pve-docs/api-viewer/#/pools
options:
  name:
    description:
      - The role name.
    type: str
    required: true
  state:
    description:
      - Specify if the role should exist (present) or absent.
    type: str
    choices: [ absent, present ]
    default: present
  privs:
    description:
      - List of privileges to give.
    type: list
    elements: str
  append:
    description:
      - Optionally append, instead of replace, privileges.
    type: bool
"""

EXAMPLES = r"""
- name: Create role 'example'
  julien_lecomte.proxmox.role:
    name: 'example'
    privs:
      - Pool.Allocate
      - Pool.Audit

- name: Append privilege to role 'example'
  julien_lecomte.proxmox.role:
    name: 'example'
    append: true
    privs:
      - SDN.Audit
"""

RETURN = r"""
name:
  description: Proxmox group name.
  returned: always
  type: str
state:
  description: State ("absent" or "present").
  returned: always
  type: str
privs:
  description: List of role privileges
  returned: when role exists
  type: list
  elements: str
special:
  description: If role is special (builtin).
  returned: when role exists
  type: int
"""


class ProxmoxRole(ProxmoxShell):
    def on_load(self):
        values = [k for k, v in self.data.items() if v == 1]
        self.data = {k: v for k, v in self.data.items() if v != 1}
        self.data["privs"] = sorted(values)

    def on_create_cmdline(self, cmd):
        cmd.extend(["-roleid", self.params["name"]])
        return self.on_modify_cmdline(cmd)

    def on_modify_cmdline(self, cmd):
        # we can't append in create mode, so disable the flag
        if self.data["state"] == "absent":
            self.params.pop("append", None)

        privs = self.params.get("privs") or []

        cmd.extend(self.maybe_command("append"))
        cmd.extend(["--privs", ",".join(privs)])

        return cmd


def main():
    module = ProxmoxRole(
        argument_spec=dict(
            name=dict(type="str", required=True),
            state=dict(type="str", default="present", choices=["absent", "present"]),
            privs=dict(type="list"),
            append=dict(type="bool"),
        ),
        supports_check_mode=False,
    )
    module.params["roleid"] = module.params["name"]
    module.run(f"/access/roles/{module.params['name']}", "/access/roles")
    module.exit_json(changed=module.changed, warnings=module.warnings, role=module.data)


if __name__ == "__main__":
    main()
