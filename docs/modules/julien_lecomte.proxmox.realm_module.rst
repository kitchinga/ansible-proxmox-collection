
.. Document meta

:orphan:

.. |antsibull-internal-nbsp| unicode:: 0xA0
    :trim:

.. role:: ansible-attribute-support-label
.. role:: ansible-attribute-support-property
.. role:: ansible-attribute-support-full
.. role:: ansible-attribute-support-partial
.. role:: ansible-attribute-support-none
.. role:: ansible-attribute-support-na
.. role:: ansible-option-type
.. role:: ansible-option-elements
.. role:: ansible-option-required
.. role:: ansible-option-versionadded
.. role:: ansible-option-aliases
.. role:: ansible-option-choices
.. role:: ansible-option-choices-default-mark
.. role:: ansible-option-default-bold
.. role:: ansible-option-configuration
.. role:: ansible-option-returned-bold
.. role:: ansible-option-sample-bold

.. Anchors

.. _ansible_collections.julien_lecomte.proxmox.realm_module:

.. Anchors: short name for ansible.builtin

.. Anchors: aliases



.. Title

realm
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. Collection note

.. note::
    This module is part of the `julien_lecomte.proxmox collection <https://galaxy.ansible.com/julien_lecomte/proxmox>`_.

    To install it, use: :code:`ansible-galaxy collection install julien\_lecomte.proxmox`.

    To use it in a playbook, specify: :code:`julien_lecomte.proxmox.realm`.

.. version_added


.. contents::
   :local:
   :depth: 1

.. Deprecated


Synopsis
--------

.. Description

- Adds, modifies, or removes a Proxmox realm.
- Returned values will exist in a variable named 'realm'.


.. Aliases


.. Requirements






.. Options

Parameters
----------


.. rst-class:: ansible-option-table

.. list-table::
  :width: 100%
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Comments

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-acr_values"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-acr_values:

      .. rst-class:: ansible-option-title

      **acr_values**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-acr_values" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Specifies the Authentication Context Class Reference values that theAuthorization Server is being requested to use for the Auth Request.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-autocreate"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-autocreate:

      .. rst-class:: ansible-option-title

      **autocreate**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-autocreate" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`boolean`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Automatically create users if they do not exist.


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry:`false`
      - :ansible-option-choices-entry:`true`


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-base_dn"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-base_dn:

      .. rst-class:: ansible-option-title

      **base_dn**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-base_dn" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      LDAP base domain name.

      Required when type is 'ldap'.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-bind_dn"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-bind_dn:

      .. rst-class:: ansible-option-title

      **bind_dn**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-bind_dn" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      LDAP bind domain name.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-capath"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-capath:

      .. rst-class:: ansible-option-title

      **capath**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-capath" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Path to the CA certificate store.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-case_sensitive"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-case_sensitive:

      .. rst-class:: ansible-option-title

      **case_sensitive**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-case_sensitive" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`boolean`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Username is case-sensitive.


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry:`false`
      - :ansible-option-choices-entry:`true`


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-cert"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-cert:

      .. rst-class:: ansible-option-title

      **cert**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-cert" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Path to the client certificate.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-certkey"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-certkey:

      .. rst-class:: ansible-option-title

      **certkey**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-certkey" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Path to the client certificate key.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-client_id"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-client_id:

      .. rst-class:: ansible-option-title

      **client_id**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-client_id" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      OpenID Client ID.

      Required when type is 'openid'.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-client_key"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-client_key:

      .. rst-class:: ansible-option-title

      **client_key**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-client_key" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      OpenID Client Key.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-comment"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-comment:

      .. rst-class:: ansible-option-title

      **comment**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-comment" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Optionally sets the comment field.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-default"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-default:

      .. rst-class:: ansible-option-title

      **default**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-default" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`boolean`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Use this as default realm.


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry:`false`
      - :ansible-option-choices-entry:`true`


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-domain"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-domain:

      .. rst-class:: ansible-option-title

      **domain**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-domain" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      AD domain name.

      Required when type is 'ad'.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-filter"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-filter:

      .. rst-class:: ansible-option-title

      **filter**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-filter" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      LDAP filter for user sync.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-group_classes"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-group_classes:

      .. rst-class:: ansible-option-title

      **group_classes**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-group_classes" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      The objectclasses for groups.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-group_dn"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-group_dn:

      .. rst-class:: ansible-option-title

      **group_dn**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-group_dn" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      LDAP base domain name for group sync.

      If not set, the base\_dn will be used.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-group_filter"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-group_filter:

      .. rst-class:: ansible-option-title

      **group_filter**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-group_filter" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      LDAP filter for group sync.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-group_name_attr"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-group_name_attr:

      .. rst-class:: ansible-option-title

      **group_name_attr**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-group_name_attr" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      LDAP attribute representing a groups name.

      If not set or found, the first value of the DN will be used as name.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-issuer_url"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-issuer_url:

      .. rst-class:: ansible-option-title

      **issuer_url**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-issuer_url" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      OpenID Issuer URL.

      Required when type is 'openid'.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-mode"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-mode:

      .. rst-class:: ansible-option-title

      **mode**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-mode" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      LDAP protocol mode.


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry:`"ldap"`
      - :ansible-option-choices-entry:`"ldap+starttls"`
      - :ansible-option-choices-entry:`"ldaps"`


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-name"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-name:

      .. rst-class:: ansible-option-title

      **name**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-name" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string` / :ansible-option-required:`required`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      The realm name.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-password"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-password:

      .. rst-class:: ansible-option-title

      **password**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-password" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      LDAP bind password. Will be stored in /etc/pve/priv/realm/\<REALM\>.pw.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-port"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-port:

      .. rst-class:: ansible-option-title

      **port**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-port" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`integer`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Server port.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-prompt"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-prompt:

      .. rst-class:: ansible-option-title

      **prompt**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-prompt" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Specifies whether the Authorization Server prompts the End-User for reauthentication and consent. (?:none|login|consent|select\_account|\\S+)


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-scopes"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-scopes:

      .. rst-class:: ansible-option-title

      **scopes**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-scopes" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      \ :strong:`Warning:`\  not to be confused with parameter \ :emphasis:`scope`\  (singular).

      Specifies the scopes (user details) that should be authorized and returned, for example email or profile.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-secure"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-secure:

      .. rst-class:: ansible-option-title

      **secure**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-secure" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`boolean`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Use ssl.


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry:`false`
      - :ansible-option-choices-entry:`true`


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-server1"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-server1:

      .. rst-class:: ansible-option-title

      **server1**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-server1" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Server IP address (or DNS name).

      Required when type is 'ad' or 'ldap'.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-server2"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-server2:

      .. rst-class:: ansible-option-title

      **server2**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-server2" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Fallback Server IP address (or DNS name).


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-sslversion"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-sslversion:

      .. rst-class:: ansible-option-title

      **sslversion**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-sslversion" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      LDAPS TLS/SSL version. It's not recommended to use version older than 1.2!


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry:`"tlsv1"`
      - :ansible-option-choices-entry:`"tlsv1\_1"`
      - :ansible-option-choices-entry:`"tlsv1\_2"`
      - :ansible-option-choices-entry:`"tlsv1\_3"`


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-state"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-state:

      .. rst-class:: ansible-option-title

      **state**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-state" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Specify if the realm should exist (present) or absent.


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry:`"absent"`
      - :ansible-option-choices-entry-default:`"present"` :ansible-option-choices-default-mark:`← (default)`


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-sync"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-sync:

      .. rst-class:: ansible-option-title

      **sync**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-sync" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`boolean`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      For a sync command to automatically sync users and groups for LDAP-based realms (LDAP & Microsoft Active Directory only)

      This requires the correct fields to be set.

      Please refer to the syncing sections of \`pveum man page \<https://pve.proxmox.com/pve-docs/pveum.1.html\>\`\_\_.


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry-default:`false` :ansible-option-choices-default-mark:`← (default)`
      - :ansible-option-choices-entry:`true`


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-sync_options"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-sync_options:

      .. rst-class:: ansible-option-title

      **sync_options**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-sync_options" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`dictionary`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Optional fields for when \ :emphasis:`sync`\  is true


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-indent"></div><div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-sync_options/dry_run"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-sync_options/dry_run:

      .. rst-class:: ansible-option-title

      **dry_run**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-sync_options/dry_run" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`boolean`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-indent-desc"></div><div class="ansible-option-cell">

      Only when \ :emphasis:`sync`\  is true.

      If set, does not write anything.


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry-default:`false` :ansible-option-choices-default-mark:`← (default)`
      - :ansible-option-choices-entry:`true`


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-indent"></div><div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-sync_options/enable_new"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-sync_options/enable_new:

      .. rst-class:: ansible-option-title

      **enable_new**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-sync_options/enable_new" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`boolean`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-indent-desc"></div><div class="ansible-option-cell">

      Only when \ :emphasis:`sync`\  is true.

      Enable newly synced users immediately.

      Defaults to true unless set in \ :emphasis:`sync\_defaults\_options`\ .


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry:`false`
      - :ansible-option-choices-entry:`true`


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-indent"></div><div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-sync_options/full"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-sync_options/full:

      .. rst-class:: ansible-option-title

      **full**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-sync_options/full" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`boolean`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-indent-desc"></div><div class="ansible-option-cell">

      Only when \ :emphasis:`sync`\  is true.

      \ :strong:`Deprecated since Proxmox 7.2`\

      If set, uses the LDAP Directory as source of truth, deleting users or groups not returned from the sync and removing all locally modified properties of synced users.

      If not set, only syncs information which is present in the synced data, and does not delete or modify anything else.

      Must either be specified in task, or set with \ :emphasis:`sync\_defaults\_options`\ .


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry:`false`
      - :ansible-option-choices-entry:`true`


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-indent"></div><div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-sync_options/purge"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-sync_options/purge:

      .. rst-class:: ansible-option-title

      **purge**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-sync_options/purge" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`boolean`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-indent-desc"></div><div class="ansible-option-cell">

      Only when \ :emphasis:`sync`\  is true.

      \ :strong:`Deprecated since Proxmox 7.2`\

      Remove ACLs for users or groups which were removed from the config during a sync.

      Must either be specified in task, or set with \ :emphasis:`sync\_defaults\_options`\ .


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry:`false`
      - :ansible-option-choices-entry:`true`


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-indent"></div><div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-sync_options/remove_vanished"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-sync_options/remove_vanished:

      .. rst-class:: ansible-option-title

      **remove_vanished**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-sync_options/remove_vanished" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-indent-desc"></div><div class="ansible-option-cell">

      Only when \ :emphasis:`sync`\  is true.

      \ :strong:`New since Proxmox 7.2`\

      A semicolon-separated list of things to remove when they or the user vanishes during a sync.

      The following values are possible: \ :literal:`entry`\  removes the user/group when not returned from the sync.

      \ :literal:`properties`\  removes the set properties on existing user/group that do not appear in the source (even custom ones).

      \ :literal:`acl`\  removes acls when the user/group is not returned from the sync.

      Must either be specified in task, or set with \ :emphasis:`sync\_defaults\_options`\ .


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-indent"></div><div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-sync_options/scope"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-sync_options/scope:

      .. rst-class:: ansible-option-title

      **scope**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-sync_options/scope" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-indent-desc"></div><div class="ansible-option-cell">

      \ :strong:`Warning:`\  not to be confused with parameter \ :emphasis:`scopes`\  (plural).

      Select what to sync.


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry:`"both"`
      - :ansible-option-choices-entry:`"groups"`
      - :ansible-option-choices-entry:`"users"`


      .. raw:: html

        </div>


  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-type"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-type:

      .. rst-class:: ansible-option-title

      **type**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-type" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Realm type.


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry:`"ad"`
      - :ansible-option-choices-entry:`"ldap"`
      - :ansible-option-choices-entry:`"openid"`
      - :ansible-option-choices-entry:`"pam"`
      - :ansible-option-choices-entry:`"pve"`


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-user_attr"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-user_attr:

      .. rst-class:: ansible-option-title

      **user_attr**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-user_attr" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      LDAP user attribute name.

      Required when type is 'ldap'.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-user_classes"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-user_classes:

      .. rst-class:: ansible-option-title

      **user_classes**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-user_classes" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      The objectclasses for users. (default = \ :literal:`inetorgperson, posixaccount, person, user`\ )


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-username_claim"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-username_claim:

      .. rst-class:: ansible-option-title

      **username_claim**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-username_claim" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      OpenID claim used to generate the unique username.


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-verify"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__parameter-verify:

      .. rst-class:: ansible-option-title

      **verify**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-verify" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`boolean`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Verify the server’s SSL certificate.


      .. rst-class:: ansible-option-line

      :ansible-option-choices:`Choices:`

      - :ansible-option-choices-entry:`false`
      - :ansible-option-choices-entry:`true`


      .. raw:: html

        </div>


.. Attributes


.. Notes


.. Seealso

See Also
--------

.. seealso::

   `PVE API <https://pve.proxmox.com/pve-docs/api-viewer/#/access/domains>`_
       Proxmox VE Application Programming Interface

.. Examples

Examples
--------

.. code-block:: yaml+jinja


    - name: Create Active Directory realm.
      julien_lecomte.proxmox.realm:
        name: corp
        type: ad
        domain: corp.example.com
        server1: corp.example.com
        default: true
        secure: true
        comment: "Example Active Directory (corp.example.com)"

    - name: Sync Active Directory groups.
      julien_lecomte.proxmox.realm:
        name: corp
        sync: True
        sync_options:
          scope: "groups"
          full: False
          purge: False




.. Facts


.. Return values

Return Values
-------------
Common return values are documented :ref:`here <common_return_values>`, the following are the fields unique to this module:

.. rst-class:: ansible-option-table

.. list-table::
  :width: 100%
  :widths: auto
  :header-rows: 1

  * - Key
    - Description

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="return-comment"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__return-comment:

      .. rst-class:: ansible-option-title

      **comment**

      .. raw:: html

        <a class="ansibleOptionLink" href="#return-comment" title="Permalink to this return value"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Comment field.


      .. rst-class:: ansible-option-line

      :ansible-option-returned-bold:`Returned:` when realm exists


      .. raw:: html

        </div>


  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="return-name"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__return-name:

      .. rst-class:: ansible-option-title

      **name**

      .. raw:: html

        <a class="ansibleOptionLink" href="#return-name" title="Permalink to this return value"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Proxmox realmid.


      .. rst-class:: ansible-option-line

      :ansible-option-returned-bold:`Returned:` always


      .. raw:: html

        </div>


  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="return-state"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__return-state:

      .. rst-class:: ansible-option-title

      **state**

      .. raw:: html

        <a class="ansibleOptionLink" href="#return-state" title="Permalink to this return value"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      State ("absent" or "present").


      .. rst-class:: ansible-option-line

      :ansible-option-returned-bold:`Returned:` always


      .. raw:: html

        </div>


  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="return-type"></div>

      .. _ansible_collections.julien_lecomte.proxmox.realm_module__return-type:

      .. rst-class:: ansible-option-title

      **type**

      .. raw:: html

        <a class="ansibleOptionLink" href="#return-type" title="Permalink to this return value"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Realm type. (ad, ldap, openid, pam, pve)


      .. rst-class:: ansible-option-line

      :ansible-option-returned-bold:`Returned:` when realm exists


      .. raw:: html

        </div>



..  Status (Presently only deprecated)


.. Authors

Authors
~~~~~~~

- Julien Lecomte



.. Extra links


.. Parsing errors
