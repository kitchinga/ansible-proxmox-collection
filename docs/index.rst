Ansible Proxmox modules, role and playbook
==========================================

.. toctree::
   :maxdepth: 1
   :caption: Contents

   introduction
   installation
   playbook
   role
   modules
   clusters

.. toctree::
   :maxdepth: 2
   :caption: Miscellaneous

   license
