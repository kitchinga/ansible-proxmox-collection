from pytest import mark

from ansible_collections.julien_lecomte.proxmox.plugins.modules import token


@mark.parametrize(
    "params, commands, output",
    # fmt: off
    [
      ( #-----------------------------------------------------------------------
        # absent to absent
        {"name": "example", "state": "absent", "user": "example@pam"},
        [
            ("pvesh get /access/users/example@pam/token/example --output-format=json", None),
        ],
        { "changed": False},
      ),
      ( #-----------------------------------------------------------------------
        # absent to present
        {"name": "example", "state": "present", "user": "example@pam"},
        [
            ("pvesh get /access/users/example@pam/token/example --output-format=json", None),
            ("pvesh create /access/users/example@pam/token/example", {}),
            ("pvesh get /access/users/example@pam/token/example --output-format=json", {}),
        ],
        { "changed": True},
      ),
# TODO: do unit tests
#      ( #-----------------------------------------------------------------------
#        # present to present
#        {"name": "example", "state": "present", "user": "example@pam"},
#        [
#            ("pvesh get /access/users/example@pam/token/example --output-format=json", None),
#        ],
#        { "changed": False},
#      ),
#      ( #-----------------------------------------------------------------------
#        # present to absent
#        {"name": "example", "state": "absent", "user": "example@pam"},
#        [
#            ("pvesh get /access/users/example@pam/token/example --output-format=json", None),
#        ],
#        { "changed": True},
#      ),
#      ( #-----------------------------------------------------------------------
#        # expire, fail
#        {"name": "example", "state": "present", "user": "example@pam", "expire": -1},
#        [
#            ("pvesh get /access/users/example@pam/token/example --output-format=json", None),
#        ],
#        { 'msg': 'expire must be 0 or a positive integer'},
#      ),
    ],
    # fmt: on
)
@mark.order(2)
def test_plugin_token(plugin, params, commands, output):
    plugin.run(params, commands, output, token.main)
