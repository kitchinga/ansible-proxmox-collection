from pytest import mark

from ansible_collections.julien_lecomte.proxmox.plugins.modules import pool


@mark.parametrize(
    "params, commands, output",
    # fmt: off
    [
      ( #-----------------------------------------------------------------------
        # absent to absent
        {"name": "example", "state": "absent"},
        [
            ("pvesh get /pools/example --output-format=json", None),
        ],
        { "changed": False},
      ),
      ( #-----------------------------------------------------------------------
        # absent to present
        {"name": "example", "state": "present"},
        [
            ("pvesh get /pools/example --output-format=json", None),
            ("pvesh create /pools -poolid example", {}),
            ("pvesh get /pools/example --output-format=json", {"poolid": "example"}),
        ],
        { "changed": True},
      ),
      ( #-----------------------------------------------------------------------
        # present to present
        {"name": "example", "state": "present"},
        [
            ("pvesh get /pools/example --output-format=json", {"poolid": "example"}),
        ],
        { "changed": False},
      ),
      ( #-----------------------------------------------------------------------
        # present to absent
        {"name": "example", "state": "absent"},
        [
            ("pvesh get /pools/example --output-format=json", {"poolid": "example"}),
            ("pvesh delete /pools/example", {}),
            ("pvesh get /pools/example --output-format=json", None),
        ],
        { "changed": True},
      ),
    ],
    # fmt: on
)
@mark.order(2)
def test_plugin_pool(plugin, params, commands, output):
    plugin.run(params, commands, output, pool.main)
