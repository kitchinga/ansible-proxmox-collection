from pytest import mark

from ansible_collections.julien_lecomte.proxmox.plugins.modules import realm


@mark.parametrize(
    "params, commands, output",
    # fmt: off
    [
      ( #-----------------------------------------------------------------------
        # absent to absent
        {"name": "example", "state": "absent"},
        [
            ("pvesh get /access/domains/example --output-format=json", None),
        ],
        {"changed": False},
      ),
      ( #-----------------------------------------------------------------------
        # absent to present
        {"name": "example", "state": "present"},
        [
            ("pvesh get /access/domains/example --output-format=json", None),
            ("pvesh create /access/domains -realm example", {}),
            ("pvesh get /access/domains/example --output-format=json", {"realm": "example"}),
        ],
        {"changed": True},
      ),
      ( #-----------------------------------------------------------------------
        # present to present
        {"name": "example", "state": "present"},
        [
            ("pvesh get /access/domains/example --output-format=json", {"realm": "example"}),
        ],
        {"changed": False},
      ),
      ( #-----------------------------------------------------------------------
        # present to absent
        {"name": "example", "state": "absent"},
        [
            ("pvesh get /access/domains/example --output-format=json", {"realm": "example"}),
            ("pvesh delete /access/domains/example", {}),
            ("pvesh get /access/domains/example --output-format=json", None),
        ],
        {"changed": True},
      ),
      ( #-----------------------------------------------------------------------
        # sync
        {"name": "example", "state": "present", "sync": True, "type": "ad"},
        [
            ("pvesh get /access/domains/example --output-format=json", {"realm": "example", "type": "ad"}),
            ("pveum realm sync example --dry-run 0", {}),
        ],
        {"changed": False},
      ),
    ],
    # fmt: on
)
@mark.order(2)
def test_plugin_realm(plugin, params, commands, output):
    plugin.run(params, commands, output, realm.main)
