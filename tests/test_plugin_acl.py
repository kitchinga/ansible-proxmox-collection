from pytest import mark

from ansible_collections.julien_lecomte.proxmox.plugins.modules import acl


@mark.parametrize(
    "params, commands, output",
    # fmt: off
    [
# TODO: Do unit tests
#       ( #-----------------------------------------------------------------------
#         # nothing to nothing (no groups, users, nor tokens)
#         {"path": "/", "state": "absent", "roles": [ 'PVEAdmin']},
#         [
#             ("pvesh get /access/acl --output-format=json", None),
#         ],
#         { "changed": False},
#       ),
#       ( #-----------------------------------------------------------------------
#         # absent to absent
#         {"path": "/", "state": "absent", "roles": [ 'PVEAdmin'], "groups": "example"},
#         [
#             ("pvesh get /access/acl --output-format=json", None),
#         ],
#         { "changed": False},
#       ),
#       ( #-----------------------------------------------------------------------
#         # absent to present
#         {"path": "/", "state": "present", "roles": [ 'PVEAdmin'], "groups": "example"},
#         [
#             ("pvesh get /access/acl --output-format=json", None),
#         ],
#         { "changed": True},
#       ),
#       ( #-----------------------------------------------------------------------
#         # present to present
#         {"path": "/", "state": "present", "roles": [ 'PVEAdmin'], "groups": "example"},
#         [
#             ("pvesh get /access/acl --output-format=json", None),
#         ],
#         { "changed": False},
#       ),
#       ( #-----------------------------------------------------------------------
#         # present to absent
#         {"path": "/", "state": "absent", "roles": [ 'PVEAdmin'], "groups": "example"},
#         [
#             ("pvesh get /access/acl --output-format=json", None),
#         ],
#         { "changed": True},
#       ),
    ],
    # fmt: on
)
@mark.order(2)
def test_plugin_acl(plugin, params, commands, output):
    plugin.run(params, commands, output, acl.main)
